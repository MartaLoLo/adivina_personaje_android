package com.example.p1cs;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.Vibrator;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.p1cs.utils.Constantes;

/**
 * @author marta.losada
 */
public class Jugar extends Activity {

	private Dialog dialog = null;
	private TextView tvQuestion;
	private Button yes, no, idntknow;

	private DataInputStream input;
	private DataOutputStream output;

	private String answer, chaName, chaSurname;

	private boolean correctConnection;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(Constantes.TAG_PLAY,"-->>CREATING GAME DISPLAY<<--");
		//Establecemos el layout correspondiente
		setContentView(R.layout.activity_juego);
		Log.i(Constantes.TAG_PLAY,"-->Establishing layout");
		//Se establecen el TextView de las caracter�sticas y los botones
		tvQuestion = (TextView) findViewById(R.id.textView1);
		yes = (Button) findViewById(R.id.button3);
		no = (Button) findViewById(R.id.button4);
		idntknow = (Button)findViewById(R.id.button1);
		Log.i(Constantes.TAG_PLAY,"-->Defining components");
		//Lo siguiente se realiza para permitir el funcionamiento correcto de los Threads principales en un m�vil f�sico
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		Log.i(Constantes.TAG_PLAY,"-->Physical mobile Threads restrictions");
		//Se realiza la conexi�n a trav�s del Socket
		Log.i(Constantes.TAG_PLAY,"-->>CREATING CONNECTION<<--");
		//Conectamos al Cliente con el Servidor insertando la ip del Servidor y en el puerto en el que est� escuchando
		new cargaConexion().execute("ejecutar");
	}

	//Se crea el men� del juego con las opciones Nuevo y Acerca De
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.juego, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.nuevo:
			//En el caso de que se haga click en Nuevo se iniciar� una nueva partida
			nuevo();
			return true;
		case R.id.acercaDe:
			//En el caso de que se haga click en Acerca De se mostrar� la informaci�n sobre la aplicaci�n
			acercaDe();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Generates question name and sends the answer to the server
	 * @param correctConnection
	 */
	public void pregunta(boolean correctConnection) {
		if(correctConnection == true){
			try {
				//Se recibe la pregunta a mostrar o el final del juego
				answer = input.readUTF();
				//se pone en el TextView para que se muestre por pantalla
				tvQuestion.setText(answer);
				//Si el usuario hace click en si
				yes.setOnClickListener(new OnClickListener() {
					public void onClick(View arg0) {
						try {
							//Se env�a al Servidor la palabra clave s� y se mira si se recibe la soluci�n
							output.writeUTF(Constantes.YES);
							solucion(input);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							Log.e(Constantes.TAG_PLAY,e.getMessage());
						}
					}
				});
				//Si el usuario hace click en no
				no.setOnClickListener(new OnClickListener() {
					public void onClick(View arg0) {
						try {
							//Se env�a al Servidor la palabra clave no y se mira si se recibe la soluci�n
							output.writeUTF(Constantes.NO);
							solucion(input);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							Log.e(Constantes.TAG_PLAY,e.getMessage());
						}
					}
				});
				//Si el usuario hace click en no lo se
				idntknow.setOnClickListener(new OnClickListener() {
					public void onClick(View arg0) {
						try {
							//Se env�a al Servidor la palabra clave no y se mira si se recibe la soluci�n
							output.writeUTF(Constantes.IDNTKNOW);
							solucion(input);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							Log.e(Constantes.TAG_PLAY,e.getMessage());
						}
					}
				});
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				Log.e(Constantes.TAG_PLAY,e1.toString());
			}
		}
	}

	/**
	 * Generates a new game
	 */
	private void nuevo() {
		//Se inicia un nuevo juego con el layout inicial y estableciendo de nuevo el TextView y los botones, adem�s de establecer la Pol�tica para permitir la conexi�n
		//con m�viles reales y por �ltimo realizar la conexi�n y establecer la pregunta que debe aparecer por pantalla
		setContentView(R.layout.activity_juego);
		tvQuestion = (TextView) findViewById(R.id.textView1);
		yes = (Button) findViewById(R.id.button3);
		no = (Button) findViewById(R.id.button4);
		idntknow = (Button)findViewById(R.id.button1);
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		new cargaConexion().execute("ejecutar");
		pregunta(correctConnection);
	}

	/**
	 * Generates the about layout
	 */
	private void acercaDe() {
		//Se abre una Activity con la informaci�n correspondiente
		Intent intent = new Intent(Jugar.this, AcercaDe.class);
		startActivity(intent);
	}

	/**
	 * Checks the data from the server and establish a corresponding behaviour:
	 *  - If the name of the person is recieved it shows a guessed dialog
	 *  - If a question is recieved it retrieves the same layout with other question
	 *  - If the name of the person is not recieved it shows a no guessed dialog
	 * @param e - Data recieved from the server
	 */
	public void solucion(DataInputStream e) {
		try {
			//Comprobar la palabra clave que se recibe del Servidor
			answer = e.readUTF();
			//Si se recibe "person" del Servidor
			if ((answer.equals(Constantes.PERSON))) {
				//entonces mostrar adivina con nombre y apellido del personaje recibido tambi�n del Servidor
				String personaje = e.readUTF();
				dialogoAdivina(personaje);
				//Si se recibe "question"
			} else if((answer.equals(Constantes.QUESTION))){
				//entonces seguir preguntando
				pregunta(correctConnection);
				//Si se recibe "noresult"
			} else if((answer.equals(Constantes.NO_RESULT))){
				//entonces mostrar el di�logo perder
				dialogoPerder();
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			Log.e(Constantes.TAG_PLAY,e1.toString());
		}
	}

	/**
	 * Establishes the dialog when the character is guessed recieving the name of the character to set it in the dialog
	 * with different behaviours depending if the character is correct or not:
	 * 	- If it is the correct character it exits the game
	 *  - If it is the incorrect character it establishes a text fields that the user has to fill with the name and surname
	 *    of the character to save it in the database.
	 * @param answer
	 */
	private void dialogoAdivina(final String answer) {
		final TextView nombre;
		final Button yes, not;
		//Se establece vibraci�n cuando se adivine
		Vibrator vibra = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		vibra.vibrate(100);
		//Se crea el dialogo no permitiendo salir cancelando y estableciendo su t�tulo, su layout y especificando su TextView donde mostrar nombre y apellido y los botones
		//si y no para especificar si se ha adivinado el personaje
		dialog = new Dialog(this);
		dialog.setCancelable(false);
		dialog.setTitle(Constantes.CHARACTER_IS);
		dialog.setContentView(R.layout.adivina);
		nombre = (TextView) dialog.findViewById(R.id.textView2);
		nombre.setText(answer);
		yes = (Button) dialog.findViewById(R.id.button5);
		//Si el usuario dice que el personaje es correcto
		yes.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				//cerramos el di�logo
				dialog.dismiss();
				try {
					//y se env�a la palabra clave correcto al Servidor para que sepa que no se va a insertar nada
					output.writeUTF(Constantes.CORRECT);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					Log.e(Constantes.TAG_PLAY,e.getMessage());
				}
				//Cerramos la ventana correspondiente al juego volviendo al men� principal
				Jugar.this.finish();
			}
		});
		not = (Button) dialog.findViewById(R.id.button6);
		//Si el usuario dice que el personaje no es el correcto
		not.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				//Se ocultan el TextView y los botones
				nombre.setTextColor(Color.TRANSPARENT);
				yes.setVisibility(View.INVISIBLE);
				not.setVisibility(View.INVISIBLE);
				//Y se ponen visibles los TextView y los EditText para la inserci�n del personaje adem�s del bot�n de aceptar
				TextView nombreI = (TextView) dialog.findViewById(R.id.textView3);
				TextView apellidoI = (TextView) dialog.findViewById(R.id.textView4);
				nombreI.setVisibility(View.VISIBLE);
				apellidoI.setVisibility(View.VISIBLE);
				final EditText intrNombre = (EditText) dialog.findViewById(R.id.editText1);
				final EditText intrApellido = (EditText) dialog.findViewById(R.id.editText2);
				intrNombre.setEnabled(true);
				intrNombre.setVisibility(View.VISIBLE);
				intrApellido.setEnabled(true);
				intrApellido.setVisibility(View.VISIBLE);
				final Button accept = (Button) dialog.findViewById(R.id.button7);
				accept.setVisibility(View.VISIBLE);
				//Establecemos un toast para que se introduzcan tanto el nombre como el apellido de forma obligatoria
				Toast.makeText(Jugar.this, Constantes.INTRO_DATA, Toast.LENGTH_LONG).show();
				//Si se hace click en aceptar
				accept.setOnClickListener(new OnClickListener() {
					public void onClick(View arg0) {
						try {
							//Se recogen los valores del nombre y del apellido
							chaName = intrNombre.getText().toString();
							chaSurname = intrApellido.getText().toString();
							//Si vienen vacios
							if(chaName.equals(Constantes.VOID_STRING)&&chaSurname.equals(Constantes.VOID_STRING)||
									chaName.equals(Constantes.VOID_STRING)||chaSurname.equals(Constantes.VOID_STRING)){
								//mostrar el mismo toast que antes para obligar a insertar los datos
								Toast.makeText(Jugar.this, Constantes.INTRO_DATA, Toast.LENGTH_LONG).show();
								//Si no vienen vac�os
							}else{
								//Enviar al Servidor el nombre y apellido para que se puedan insertar
								output.writeUTF(Constantes.NAME_FORMAT + chaName + Constantes.SURNAME_FORMAT + chaSurname);
								//Se cierra el dialogo
								dialog.dismiss();
								//Se cierra la ventana del juego y se vuelve a la principal
								Jugar.this.finish();
							}
						} catch (IOException e) {
							// TODO Auto-generated catch block
							Log.e(Constantes.TAG_PLAY,e.getMessage());
						}
					}
				});
			}
		});
		//Mostramos el dialogo adivina
		dialog.show();
	}

	/**
	 * Establishes the dialog when it not guesses the character so, it establishes a text fields that the user has to fill 
	 * with the name and surname of the character to save it in the database.
	 */
	private void dialogoPerder() {
		//Se establece una vibraci�n cuando no se adivina
		Vibrator vibra = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		vibra.vibrate(100);
		//Se crea el dialogo, no se permite que se cancele, se pone su t�tulo y el layout correspondiente
		dialog = new Dialog(this);
		dialog.setCancelable(false);
		dialog.setTitle(Constantes.NO_CHARACTER_IS);
		dialog.setContentView(R.layout.pierde);
		//adem�s establecemos los TextView para introducir y el bot�n para aceptar la introducci�n del personaje
		final EditText intrNombre = (EditText) dialog.findViewById(R.id.editText3);
		final EditText intrApellido = (EditText) dialog.findViewById(R.id.editText4);
		final Button ok = (Button) dialog.findViewById(R.id.button8);
		//Establecemos un toast para que se introduzcan tanto el nombre como el apellido de forma obligatoria
		Toast.makeText(Jugar.this, Constantes.INTRO_DATA, Toast.LENGTH_LONG).show();
		//Si se hace click en aceptar
		ok.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				try {
					//Se recogen los valores del nombre y del apellido
					chaName = intrNombre.getText().toString();
					chaSurname = intrApellido.getText().toString();
					//Si vienen vacios
					if(chaName.equals(Constantes.VOID_STRING)&&chaSurname.equals(Constantes.VOID_STRING)||
							chaName.equals(Constantes.VOID_STRING)||chaSurname.equals(Constantes.VOID_STRING)){
						//mostrar el mismo toast que antes para obligar a insertar los datos
						Toast.makeText(Jugar.this, Constantes.INTRO_DATA, Toast.LENGTH_LONG).show();
						//Si no vienen vac�os
					}else{
						//Enviar al Servidor el nombre y apellido para que se puedan insertar
						output.writeUTF(Constantes.NAME_FORMAT + chaName + Constantes.SURNAME_FORMAT + chaSurname);
						//Cerramos el di�logo
						dialog.dismiss();
						//Cerramos la ventana del juego y volvemos al men� principal
						Jugar.this.finish();
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					Log.e(Constantes.TAG_PLAY,e.getMessage());
				}
			}
		});
		//Mostramos el di�logo de no adivinar
		dialog.show();
	}

	/**
	 * Establishes a asynchronous task that has to put a ProgressDialog while the connection is opening. 
	 *  - In case that it is not connected, it quits the ProgressDialog and then retrieves a Toast informing of that.
	 *  - In case that it is connected, it quits the ProgressDialog and then retrieves a question.
	 */
	private class cargaConexion extends AsyncTask<String, Float, Boolean> {

		private ProgressDialog ringProgressDialog;

		@Override
		//Este m�todo ser� el encargado de realizar la tarea en segundo plano
		protected Boolean doInBackground(String... params) {

			if(params[0].equals("ejecutar"))
			{
				try {
					try {
						Thread.sleep(4000);
					} catch (InterruptedException e) {
						Log.e(Constantes.TAG_PLAY,e.getMessage());
					}
					Socket conexion = new Socket();
					conexion.connect(new InetSocketAddress(Constantes.IP, Constantes.PORT), 10*1000);
					//Creamos la entrada y la salida
					input = new DataInputStream(conexion.getInputStream());
					output = new DataOutputStream(conexion.getOutputStream());
					correctConnection = true;
				} catch (SocketException e) {
					Log.e(Constantes.TAG_PLAY,e.getMessage());
					correctConnection = false;
				} catch  (IOException e) {
					Log.e(Constantes.TAG_PLAY, e.getMessage());
					correctConnection = false;
				}
			}
			return correctConnection;

		}

		//Este m�todo se ejecuta en otro hilo, por lo que no podremos modificar la UI desde �l. Para ello, usaremos los tres m�todos siguientes.
		@Override
		//Este m�todo se ejecutar� antes de doInBackground, por lo que podremos modificar la interfaz para indicar el comienzo de la tarea (colocar un  cargando, desactivar botones�).
		protected void onPreExecute() {
			ringProgressDialog = ProgressDialog.show(Jugar.this, null, null, true, false);
			ringProgressDialog.setContentView(R.layout.custom_progressdialog);
		}

		@Override
		//Este �ltimo m�todo, como habr�is supuesto, se ejecuta tras terminar doInBackground y recibe como par�metro lo que este devuelva.
		protected void onPostExecute(Boolean conexionCorrecta) {
			if(conexionCorrecta == true){
				//Se establece la pregunta en el TextView en el caso de que la conexi�n sea correcta
				Log.i(Constantes.TAG_PLAY,"-->>CREATING QUESTION<<--");
				pregunta(conexionCorrecta);
				//Se elimina la carga
				ringProgressDialog.dismiss();
			}
			else{
				ringProgressDialog.dismiss();
				Toast.makeText(Jugar.this, Constantes.NO_CONNECTION, Toast.LENGTH_LONG).show();
				Jugar.this.finish();
			}
		}

	}

}
