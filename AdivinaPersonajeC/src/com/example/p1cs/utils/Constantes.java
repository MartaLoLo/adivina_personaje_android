package com.example.p1cs.utils;

/**
 * @author marta.losada
 */
public class Constantes {

	//Gen�ricos
	public static String VOID_STRING       	= "";
	
	//Jugar
	public static final String TAG_PLAY 		= "Jugar";
	public static final String IP 				= "192.168.0.157";
	public static final int    PORT 			= 2812;
	public static final String NO_CONNECTION    = "Ha ocurrido un error int�ntelo de nuevo m�s tarde";
	public static final String YES				= "si";
	public static final String NO				= "no";
	public static final String IDNTKNOW			= "nose";
	public static final String PERSON     		= "person";
	public static final String QUESTION  	 	= "question";
	public static final String NO_RESULT   		= "noresult";
	public static final String CHARACTER_IS		= "Su personaje es...";
	public static final String CORRECT       	= "correcto";
	public static final String INTRO_DATA   	= "Introduzca tanto el nombre como el apellido del personaje";
	public static final String NAME_FORMAT  	= "Nombre#";
	public static final String SURNAME_FORMAT	= "$Apellido#";
	public static final String NO_CHARACTER_IS	= "No se ha podido adivinar su personaje...";
	
	//MainActivity
	public static final String TAG_MAIN 		= "MainActivity";
	public static final String EXIT 			= "Salir";
	public static final String QEXIT			= "�Esta seguro que desea salir?";
	public static final String NO_M				= "No";
	public static final String YES_M			= "Si";
	
	//AcercaDe
	public static final String TAG_ABOUT		= "AcercaDe";
	
	//Hilo
	public static final String USER             = "root";
	public static final String PASSWORD         = "Yusv.wH3";
	public static final int    NUM_CHARAC		= 107;
	public static final String NAME				= "Nombre";
	public static final String RUTE_JDBC        = "jdbc:mysql://localhost/adivina_personaje";
	
}
