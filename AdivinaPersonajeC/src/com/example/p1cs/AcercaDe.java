package com.example.p1cs;

import com.example.p1cs.utils.Constantes;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

/**
 * @author marta.losada
 */
public class AcercaDe extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(Constantes.TAG_ABOUT,"-->>CREATING LAYOUT ACERCA DE...<<--");
		//Establecemos el layout correspondiente
		setContentView(R.layout.activity_acerca_de);
	}
	
}
