package com.example.p1cs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.example.p1cs.utils.Constantes;

/**
 * @author marta.losada
 */
public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(Constantes.TAG_MAIN,"-->>CREATING INITIAL DISPLAY<<--");
		Log.i(Constantes.TAG_MAIN,"-->Allocating layout");
		setContentView(R.layout.activity_main);
		//Definimos el bot�n de jugar y abrimos el juego en s�
		Log.i(Constantes.TAG_MAIN,"-->Defining buttons");
		Button btnJuego=(Button)findViewById(R.id.button10);
		Log.i(Constantes.TAG_MAIN,"-->Determining buttons' action listener");
		btnJuego.setOnClickListener(new OnClickListener(){
			public void onClick(View arg0) {
				Intent intent = new Intent(MainActivity.this, Jugar.class);	
				startActivity(intent);
			}
		});
		//Definimos el bot�n salir para salir del men� principal y del juego
		Button btnSalir=(Button)findViewById(R.id.button2);
		btnSalir.setOnClickListener(new OnClickListener(){
			public void onClick(View arg0) {
				dialogoSalir();
			}
		});

	}

	//Establecemos el men� que en este caso ser� innecesario por ello se devuelve false
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return false;
	}

	public void dialogoSalir()
	{
		Log.i(Constantes.TAG_MAIN,"-->>CREATING EXIT DIALOG<<--");
		//Establecemos la vibraci�n cuando salimos
		Log.i(Constantes.TAG_MAIN,"-->Establishing vibration");
		Vibrator vibra=(Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
		vibra.vibrate(200);
		//Creamos un dialogo de alerta con su t�tulo y su pregunta correspondiente, as� como los botones
		//de si se desea salir o no
		AlertDialog.Builder dialogo=new AlertDialog.Builder(this);
		Log.i(Constantes.TAG_MAIN,"-->Establishing title");
		dialogo.setTitle(Constantes.EXIT);
		Log.i(Constantes.TAG_MAIN,"-->Establishing content");
		dialogo.setMessage(Constantes.QEXIT);
		//Si el usuario dice que no quiere salir
		Log.i(Constantes.TAG_MAIN,"-->Establishing buttons and its listeners");
		dialogo.setNegativeButton(Constantes.NO_M, new DialogInterface.OnClickListener(){
			public void onClick(DialogInterface dialog, int which){
				//simplemente salimos del di�logo de alerta
				dialog.cancel();
			}
		});
		//Si el usuario dice que si quiere salir
		dialogo.setPositiveButton(Constantes.YES_M, new DialogInterface.OnClickListener(){
			public void onClick(DialogInterface dialog, int which){
				//cerramos el juego
				MainActivity.this.finish();
			}
		});
		//Mostramos el di�logo de alerta
		Log.i(Constantes.TAG_MAIN,"-->Showing dialog");
		dialogo.show();
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		Log.i(Constantes.TAG_MAIN,"-->>CREATING ACTION BACK ARROW<<--");
		//Cuando hacemos click en la flecha atr�s estando en el men� principal
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			//Se muestra el di�logo salir 
			Log.i(Constantes.TAG_MAIN,"-->Showing exit dialog");
			dialogoSalir();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

}
