package mll.proyecto.servidor.utils;

import java.util.ArrayList;

/**
 * @author marta.losada
 */
public class Caracteristica {
	public static final ArrayList<Integer> carAfirmativas = new ArrayList<Integer>();
	
	private static final int HOMBRE 			= 0;
	private static final int MUJER 				= 1;
	private static final int CALVO 				= 2;
	private static final int BARBA_O_BIGOTE 	= 3;
	private static final int PELO_LARGO 		= 4;
	private static final int PELO_CORTO 		= 5;
	private static final int CANOSO 			= 6;
	private static final int PELIRROJO 			= 7;
	private static final int RUBIO 				= 8;
	private static final int MORENO 			= 9;
	private static final int CASTA�O 			= 10;
	private static final int POCO_COMUN 		= 11;
	private static final int OJOS_CLAROS 		= 12;
	private static final int OJOS_OSCUROS 		= 13;
	private static final int ALTO 				= 14;
	private static final int BAJO 				= 15;
	private static final int GORDO 				= 16;
	private static final int NORMAL 			= 17;
	private static final int DELGADO 			= 18;
	private static final int RAZA_BLANCA 		= 19;
	private static final int RAZA_AMARILLA 		= 20;
	private static final int RAZA_NEGRA 		= 21;
	private static final int MULATO 			= 22;
	private static final int GAFAS 				= 23;
	private static final int ACTUAL 			= 24;
	private static final int MENOS_20 			= 25;
	private static final int ENTRE_20_30 		= 26;
	private static final int ENTRE_31_40 		= 27;
	private static final int ENTRE_41_50 		= 28;
	private static final int ENTRE_51_60 		= 29;
	private static final int ENTRE_61_70 		= 30;
	private static final int MAS_70 			= 31;
	private static final int ISLA 				= 32;
	private static final int BALEARES 			= 33;
	private static final int CANARIAS 			= 34;
	private static final int CIUDAD_AUTO 		= 35;
	private static final int CEUTA 				= 36;
	private static final int MELILLA 			= 37;
	private static final int UNIPROV 			= 38;
	private static final int ASTURIAS 			= 39;
	private static final int CANTABRIA 			= 40;
	private static final int LARIOJA 			= 41;
	private static final int MADRID 			= 42;
	private static final int MURCIA 			= 43;
	private static final int NAVARRA 			= 44;
	private static final int ANDALUCIA 			= 45;
	private static final int ARAGON 			= 46;
	private static final int CASTILLA_LEON 		= 47;
	private static final int CASTILLA_MANCHA 	= 48;
	private static final int CATALUNYA 			= 49;
	private static final int VALENCIANA 		= 50;
	private static final int EXTREMADURA 		= 51;
	private static final int GALICIA 			= 52;
	private static final int PAIS_VASCO 		= 53;
	private static final int POLITICA 			= 54;
	private static final int GOBIERNO 			= 55;
	private static final int MINISTRO 			= 58;
	private static final int EXTERIORES 		= 59;
	private static final int OPOSICION 			= 71;
	private static final int PRESIDENTE 		= 72;
	private static final int SE_SINDICATO 		= 76;
	private static final int CCOO 				= 77;
	private static final int DEPORTE 			= 79;
	private static final int TENISTA 			= 80;
	private static final int BALONCESTO 		= 82;
	private static final int EN_ESPANYA 		= 83;
	private static final int NADADOR   			= 85;
	private static final int CICLISTA 			= 86;
	private static final int GANADOR_VUELTA 	= 87;
	private static final int GANADOR_TOUR 		= 88;
	private static final int PILOTO_MOTOS 		= 89;
	private static final int FUTBOLISTA 		= 91;
	private static final int PRIMERA 			= 92;
	private static final int BARCELONA 			= 93;
	private static final int SEGUNDA 			= 96;
	private static final int DEPORTIVO			= 97;
	private static final int MUSICA 			= 98;
	private static final int CANTANTE 			= 99;
	private static final int GRUPO 				= 100;
	private static final int ESPANYA 			= 101;
	private static final int PREMIOS_NACIONALES = 102;
	private static final int PREMIOS_INTERNACIO = 103;
	private static final int ACTOR_CINE			= 104;
	public  static final int NO_ADIVINADO		= 106;

	public static final Integer[] arrayCarSi()
	{
		Integer[] arraySi = new Integer[107];

		arraySi[HOMBRE]=CALVO;
		arraySi[MUJER]=PELO_LARGO;
		arraySi[CALVO]=BARBA_O_BIGOTE;
		arraySi[BARBA_O_BIGOTE]=PELO_LARGO;
		arraySi[PELO_LARGO]=CANOSO;
		arraySi[PELO_CORTO]=CANOSO;
		arraySi[CANOSO]=OJOS_CLAROS;
		arraySi[PELIRROJO]=OJOS_CLAROS;
		arraySi[RUBIO]=OJOS_CLAROS;
		arraySi[MORENO]=OJOS_CLAROS;
		arraySi[CASTA�O]=OJOS_CLAROS;
		arraySi[POCO_COMUN]=OJOS_CLAROS;
		arraySi[OJOS_CLAROS]=ALTO;
		arraySi[OJOS_OSCUROS]=ALTO;
		arraySi[ALTO]=GORDO;
		arraySi[BAJO]=GORDO;
		arraySi[GORDO]=RAZA_BLANCA;
		arraySi[NORMAL]=RAZA_BLANCA;
		arraySi[DELGADO]=RAZA_BLANCA;
		arraySi[RAZA_BLANCA]=GAFAS;
		arraySi[RAZA_AMARILLA]=GAFAS;
		arraySi[RAZA_NEGRA]=GAFAS;
		arraySi[MULATO]=GAFAS;
		arraySi[GAFAS]=ACTUAL;
		arraySi[ACTUAL]=MENOS_20;
		arraySi[MENOS_20]=ISLA;
		arraySi[ENTRE_20_30]=ISLA;
		arraySi[ENTRE_31_40]=ISLA;
		arraySi[ENTRE_41_50]=ISLA;
		arraySi[ENTRE_51_60]=ISLA;
		arraySi[ENTRE_61_70]=ISLA;
		arraySi[MAS_70]=ISLA;
		arraySi[ISLA]=BALEARES;
		arraySi[BALEARES]=POLITICA;
		arraySi[CANARIAS]=POLITICA;
		arraySi[CIUDAD_AUTO]=CEUTA;
		arraySi[CEUTA]=POLITICA;
		arraySi[MELILLA]=POLITICA;
		arraySi[UNIPROV]=ASTURIAS;
		arraySi[ASTURIAS]=POLITICA;
		arraySi[CANTABRIA]=POLITICA;
		arraySi[LARIOJA]=POLITICA;
		arraySi[MADRID]=POLITICA;
		arraySi[MURCIA]=POLITICA;
		arraySi[NAVARRA]=POLITICA;
		arraySi[ANDALUCIA]=POLITICA;
		arraySi[ARAGON]=POLITICA;
		arraySi[CASTILLA_LEON]=POLITICA;
		arraySi[CASTILLA_MANCHA]=POLITICA;
		arraySi[CATALUNYA]=POLITICA;
		arraySi[VALENCIANA]=POLITICA;
		arraySi[EXTREMADURA]=POLITICA;
		arraySi[GALICIA]=POLITICA;
		arraySi[PAIS_VASCO]=POLITICA;
		arraySi[POLITICA]=GOBIERNO;
		arraySi[GOBIERNO]=MINISTRO;
		arraySi[MINISTRO]=EXTERIORES;
		arraySi[OPOSICION]=PRESIDENTE;
		arraySi[SE_SINDICATO]=CCOO;
		arraySi[DEPORTE]=TENISTA;
		arraySi[BALONCESTO]=EN_ESPANYA;
		arraySi[CICLISTA]=GANADOR_VUELTA;
		arraySi[GANADOR_VUELTA]=GANADOR_TOUR;
		arraySi[FUTBOLISTA]=PRIMERA;
		arraySi[PRIMERA]=BARCELONA;
		arraySi[MUSICA]=CANTANTE;
		arraySi[CANTANTE]=GRUPO;
		arraySi[GRUPO]=ESPANYA;
		arraySi[ESPANYA]=PREMIOS_NACIONALES;

		return arraySi;
	}
	
	public static final Integer[] arrayCarNo()
	{
		Integer[] arrayNo = new Integer[107];

		arrayNo[HOMBRE]=PELO_LARGO;
		arrayNo[PELO_LARGO]=CANOSO;
		arrayNo[CASTA�O]=POCO_COMUN;
		arrayNo[OJOS_CLAROS]=ALTO;
		arrayNo[ALTO]=GORDO;
		arrayNo[NORMAL]=RAZA_BLANCA;
		arrayNo[ACTUAL]=ISLA;
		arrayNo[ISLA]=CIUDAD_AUTO;
		arrayNo[CIUDAD_AUTO]=UNIPROV;
		arrayNo[UNIPROV]=ANDALUCIA;
		arrayNo[POLITICA]=DEPORTE;
		arrayNo[DEPORTE]=MUSICA;
		arrayNo[BALONCESTO]=NADADOR;
		arrayNo[CICLISTA]=PILOTO_MOTOS; 
		arrayNo[FUTBOLISTA]=MUSICA;
		arrayNo[PRIMERA]=DEPORTIVO;
		arrayNo[MUSICA]=ACTOR_CINE;
		arrayNo[CANTANTE]=ESPANYA;

		return arrayNo;
	}
	
	public static final Integer[] arrayCarNoSe()
	{
		Integer[] arrayNoSe = new Integer[107];

		arrayNoSe[HOMBRE]=CALVO;
		arrayNoSe[MUJER]=CALVO;
		arrayNoSe[CALVO]=BARBA_O_BIGOTE;
		arrayNoSe[BARBA_O_BIGOTE]=PELO_LARGO;
		arrayNoSe[PELO_LARGO]=CANOSO;
		arrayNoSe[PELO_CORTO]=CANOSO;
		arrayNoSe[CANOSO]=OJOS_CLAROS;
		arrayNoSe[PELIRROJO]=OJOS_CLAROS;
		arrayNoSe[RUBIO]=OJOS_CLAROS;
		arrayNoSe[MORENO]=OJOS_CLAROS;
		arrayNoSe[CASTA�O]=OJOS_CLAROS;
		arrayNoSe[POCO_COMUN]=OJOS_CLAROS;
		arrayNoSe[OJOS_CLAROS]=ALTO;
		arrayNoSe[OJOS_OSCUROS]=ALTO;
		arrayNoSe[ALTO]=GORDO;
		arrayNoSe[BAJO]=GORDO;
		arrayNoSe[GORDO]=RAZA_BLANCA;
		arrayNoSe[NORMAL]=RAZA_BLANCA;
		arrayNoSe[DELGADO]=RAZA_BLANCA;
		arrayNoSe[RAZA_BLANCA]=GAFAS;
		arrayNoSe[RAZA_AMARILLA]=GAFAS;
		arrayNoSe[RAZA_NEGRA]=GAFAS;
		arrayNoSe[MULATO]=GAFAS;
		arrayNoSe[GAFAS]=ACTUAL;
		arrayNoSe[ACTUAL]=ISLA;
		arrayNoSe[MENOS_20]=ISLA;
		arrayNoSe[ENTRE_20_30]=ISLA;
		arrayNoSe[ENTRE_31_40]=ISLA;
		arrayNoSe[ENTRE_41_50]=ISLA;
		arrayNoSe[ENTRE_51_60]=ISLA;
		arrayNoSe[ENTRE_61_70]=ISLA;
		arrayNoSe[MAS_70]=ISLA;
		arrayNoSe[ISLA]=CIUDAD_AUTO;
		arrayNoSe[BALEARES]=POLITICA;
		arrayNoSe[CANARIAS]=POLITICA;
		arrayNoSe[CIUDAD_AUTO]=UNIPROV;
		arrayNoSe[CEUTA]=POLITICA;
		arrayNoSe[MELILLA]=POLITICA;
		arrayNoSe[ASTURIAS]=POLITICA;
		arrayNoSe[CANTABRIA]=POLITICA;
		arrayNoSe[LARIOJA]=POLITICA;
		arrayNoSe[MADRID]=POLITICA;
		arrayNoSe[MURCIA]=POLITICA;
		arrayNoSe[NAVARRA]=POLITICA;
		arrayNoSe[ANDALUCIA]=POLITICA;
		arrayNoSe[ARAGON]=POLITICA;
		arrayNoSe[CASTILLA_LEON]=POLITICA;
		arrayNoSe[CASTILLA_MANCHA]=POLITICA;
		arrayNoSe[CATALUNYA]=POLITICA;
		arrayNoSe[VALENCIANA]=POLITICA;
		arrayNoSe[EXTREMADURA]=POLITICA;
		arrayNoSe[GALICIA]=POLITICA;
		arrayNoSe[PAIS_VASCO]=POLITICA;
		arrayNoSe[POLITICA]=DEPORTE;
		arrayNoSe[GOBIERNO]=OPOSICION;
		arrayNoSe[MINISTRO]=NO_ADIVINADO;
		arrayNoSe[OPOSICION]=SE_SINDICATO;
		arrayNoSe[SE_SINDICATO]=NO_ADIVINADO;
		arrayNoSe[DEPORTE]=MUSICA;
		arrayNoSe[BALONCESTO]=NADADOR;
		arrayNoSe[NADADOR]=CICLISTA;
		arrayNoSe[CICLISTA]=PILOTO_MOTOS;
		arrayNoSe[GANADOR_VUELTA]=NO_ADIVINADO;
		arrayNoSe[FUTBOLISTA]=MUSICA;
		arrayNoSe[PRIMERA]=SEGUNDA;
		arrayNoSe[SEGUNDA]=NO_ADIVINADO;
		arrayNoSe[MUSICA]=ACTOR_CINE;
		arrayNoSe[PREMIOS_INTERNACIO]=NO_ADIVINADO;
		arrayNoSe[ACTOR_CINE]=NO_ADIVINADO;
		
		return arrayNoSe;
	}

}