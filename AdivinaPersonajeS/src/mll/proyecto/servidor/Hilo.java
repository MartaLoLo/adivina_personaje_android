package mll.proyecto.servidor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import mll.proyecto.servidor.utils.Caracteristica;

import com.example.p1cs.utils.Constantes;

/**
 * @author marta.losada
 */
public class Hilo implements Runnable {

	private ServerSocket server;
	private Socket client;

	private DataOutputStream out;
	private DataInputStream in;

	private Connection connection;
	private Statement stm;
	private ResultSet rs;
	private PreparedStatement pstmt;

	private String query, insert, querY[];
	private int number, viewCounter, counter, intermediateCounter;

	/**
	 * Hilo's constructor to establish the Server
	 * @param s - ServerSocket
	 */
	public Hilo(ServerSocket s) {
		server = s;
	}

	/**
	 * Executes query to establish the corresponding question in the client
	 * @param number
	 */
	public void ejecutaConsulta(int number){
		//Cuando ejecutamos una consulta hacemos saber al Cliente (Jugar) que se est� haciendo 
		//una pregunta para que la muestre por pantalla con la palabra clave "question"
		try {
			out.writeUTF(Constantes.QUESTION);
		} catch (IOException e) {
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, "ERROR sending QUESTION: " + e.getMessage());
		}
		//y efectuamos la consulta
		consulta(number);
	}

	/**
	 * Establishes the corresponding query, sends the name of the query, likewise his behavior receiving si, 
	 * no or no lo se that it will be send to the client
	 * @param number
	 */
	public void consulta(int number){
		String linea, resultado = Constantes.VOID_STRING;
		try { 
			//Cogemos la consulta que corresponde a la caracter�stica almacenada
			query = querY[number];
			try {
				//Ejecutamos la consulta
				stm = connection.createStatement();
				rs = stm.executeQuery(query);
				while (rs.next()) {
					resultado = rs.getString(1);
				}
			} catch (SQLException e) {
				try {
					rs.close();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					Logger.getLogger(getClass().getName()).log(Level.SEVERE, "ERROR closing ResultSet: " + e1.getMessage());
				}
				Logger.getLogger(getClass().getName()).log(Level.SEVERE, "ERROR executing query: " + e.getMessage());
			}
			//Enviamos al Cliente el resultado de la consulta para pregunta
			out.writeUTF(resultado);
			try{
				//Leemos si el Jugador ha dicho que si o que no a la caracter�stica mostrada
				linea = in.readUTF();
				if(linea !=null){
					if(!(linea.equals(Constantes.VOID_STRING))){
						//Si la respuesta a la pregunta es si, metemos la caracter�stica, en este caso su equivalente en n�mero 
						//en un ArrayList de caracter�sticas afirmativas. Tambi�n creamos la vista de la respuesta afirmativa y 
						//realizamos la l�gica de que pregunta viene despu�s en caso afirmativo
						if (linea.equals(Constantes.YES)) 
						{
							Caracteristica.carAfirmativas.add(number);
							crearVista(number);
							logicaSi(number);
						}
						//Si la respuesta a la pregunta es no, simplemente efectuamos la l�gica para saber que pregunta viene
						//despu�s as� como en caso de preguntas antag�nicas a�adir el dato a caracteristicas afirmativas y crear
						//la vista correspondiente
						if (linea.equals(Constantes.NO)) 
						{				
							logicaNo(number);
						}
						if (linea.equals(Constantes.IDNTKNOW))
						{
							logicaNose(number);
						}
					}
				} 
			}catch(IOException e){
				Logger.getLogger(getClass().getName()).log(Level.SEVERE, "ERROR reading input data: " + e.getMessage());
			}finally {
				if(out != null){
					out.flush();
					out.close();
				}if(in !=null){
					in.close();
				}if(client !=null){
					client.close();
				}
			}
		} catch (IOException e) {
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Error performing query caracter�stica: " + e.getMessage());
		}
		try {
			rs.close();
		} catch (SQLException e) {
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, "ERROR closing ResultSet: " + e.getMessage());
		}
	}

	/**
	 * Generates corresponding view of the afirmative characteristic
	 * @param afirmative
	 */
	public void crearVista(int afirmative){
		//Creaci�n de vista de caracteristicas afirmativas con el formato vista_X siendo X el n�mero de caracter�stica
		String vista="Create or replace view adivina_personaje.vista_? as select distinct per_id from adivina_personaje.tener where car_id = ?";
		if(Caracteristica.carAfirmativas.contains(afirmative)){
			try {
				//Se ejecuta la vista para crearla o reemplazarla en el caso de que existan vistas anteriores de otros
				//juegos
				pstmt = connection.prepareStatement(vista);
				pstmt.setInt(1, viewCounter+1);
				pstmt.setInt(2, afirmative+1);
				pstmt.executeUpdate();
			} catch (SQLException e) {
				try {
					pstmt.close();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					Logger.getLogger(getClass().getName()).log(Level.SEVERE, "ERROR closing PreparedStatement: " + e1.getMessage());
				}
				//En el caso de que no se haya podido crear la vista se muestra en el Servidor un error
				Logger.getLogger(getClass().getName()).log(Level.SEVERE, "ERROR,  it has not been able to create the view of the characters respect to the characteristics: " + e.getMessage());
			}
			//Se incrementa el n�mero de la vista
			viewCounter++;
			//Se crea la vista intermedia de las vistas y se efect�a el inner join de las mismas para las caracter�sticas comunes
			ejecutarInnerJoinIntermedias();
		}
	}

	/**
	 * Generates intermediate view through a InnerJoin between two views prebuilt
	 */
	public void crearVistaIntermedia(){
		String intermedia="Create or replace view adivina_personaje.intermedia_? as select distinct adivina_personaje.vista_?.per_id from adivina_personaje.vista_? inner join adivina_personaje.vista_? on adivina_personaje.vista_?.per_id = adivina_personaje.vista_?.per_id";
		try {
			//Se ejecuta la vista intermedia que conjuga las caracter�sticas comunes de las vistas dos a dos
			pstmt = connection.prepareStatement(intermedia);
			pstmt.setInt(1, viewCounter-1);
			pstmt.setInt(2, viewCounter-1);
			pstmt.setInt(3, viewCounter-1);
			pstmt.setInt(4, viewCounter);
			pstmt.setInt(5, viewCounter-1);
			pstmt.setInt(6, viewCounter);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			try {
				pstmt.close();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				Logger.getLogger(getClass().getName()).log(Level.SEVERE, "ERROR closing PreparedStatement: " + e1.getMessage());
			}
			//En el caso de que no se haya podido crear la vista intermedia se muestra un error en Servidor
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, "ERROR creating intermediate view: " + e.getMessage());
		}
		//Se incrementa el contador de vistas intermedias
		intermediateCounter++;
	}

	/**
	 * Generates results through a InnerJoin between the intermediate views
	 */
	public void ejecutarInnerJoinIntermedias(){
		String consultaInner = Constantes.VOID_STRING;
		//Si el contador de la vista es mayor que uno se crea la vista intermedia porque precisa de dos vistas para poder crearse
		if(viewCounter > 1){
			crearVistaIntermedia();
			//Si el contador de la vista intermedia es mayor que uno se efectua el inner join de las vistas intermedias ya que precisa de dos intermedias
			//para poder efectuarse
			if(intermediateCounter > 1){
				//Efectuamos el inner join de las vistas intermedias dos a dos
				String o[] = new String[12]; 
				String s[] = new String[12]; 
				String j[] = new String[12];
				for (int i = 0; i<12; i++)
				{
					s[i] = " Select distinct adivina_personaje.intermedia_"+(intermediateCounter-(i+1))+".per_id" +
							" from adivina_personaje.intermedia_"+(intermediateCounter-(i+1));
					j[i] = " inner join adivina_personaje.intermedia_"+(intermediateCounter-i);
					o[i] = " on adivina_personaje.intermedia_"+(intermediateCounter-(i+1))+".per_id = adivina_personaje.intermedia_"+(intermediateCounter-i)+".per_id";
				}
				if(intermediateCounter<s.length&&intermediateCounter>1)
				{
					consultaInner=s[intermediateCounter-2]+j[intermediateCounter-2]+o[intermediateCounter-2];
					for(int k=intermediateCounter-3;k>=0;--k)
					{
						consultaInner += j[k]+o[k];
					}
				}
				else
				{
					consultaInner = s[0]+j[0]+o[0];
				}

				try {
					//Ejecutamos el inner join
					rs = stm.executeQuery(consultaInner);
					counter = 0;
					while(rs.next()){
						number = rs.getInt(1);
						counter++;
					}
					//Si la consulta devuelve mas de un resultado
					if(counter > 1){
						//Seguir preguntando
						//Si la consulta devuelve un resultado
					}else if(counter == 1){
						//se ha adivinado el personaje por tanto se efect�a el m�todo resultado
						resultadoPersonaje(number);
						//Si la consulta no devuelve ningun resultado
					}else if(counter == 0){
						//querr� decir que no existe ning�n personaje con dichas caracter�sticas
						//Para tener un n�mero razonable de caracter�sticas a insertar, obligaremos a que sean 8 siendo el tama�o del ArrayList mayor que 8
						if(Caracteristica.carAfirmativas.size()>8){
							try {
								//Enviamos al Cliente la palabra clave "noresult" para que muestre por pantalla el dialogo de que no se ha adivinado personaje
								out.writeUTF(Constantes.NO_RESULT);
							} catch (IOException e) {
								Logger.getLogger(getClass().getName()).log(Level.SEVERE, "ERROR sending NO_RESULT: " + e.getMessage());
							}
							//y adem�s insertamos el personaje y borramos el ArrayList de caracter�sticas afirmativas para posteriores juegos
							insertarPersonaje();
							Caracteristica.carAfirmativas.clear();
						}
					}
				} catch (SQLException e) {
					try {
						rs.close();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						Logger.getLogger(getClass().getName()).log(Level.SEVERE, "ERROR closing ResultSet: " + e1.getMessage());
					}
					//En el caso de que no se haya podido hacer el join de las vistas intermedias se muestra un error en el Servidor
					Logger.getLogger(getClass().getName()).log(Level.SEVERE, "ERROR, it has not been able to create the join of the intermediate characteristics: " + e.getMessage());
				}
			}
		}
	}

	/**
	 * Afirmative logic for all characteristics to establish their subsequent behavior
	 * @param number
	 */
	public void logicaSi(int number){
		//Por ejemplo en el primer caso si es hombre, preguntar si es calvo
		//y si es mujer preguntar si tiene pelo largo
		Integer[] arraySi = Caracteristica.arrayCarSi();

		if(arraySi[number]!=null)
		{
			ejecutaConsulta(arraySi[number]);
		}

	}

	/**
	 * Negative logic for all characteristics to establish their subsequent behavior
	 * @param number
	 */
	public void logicaNo(int number){
		Integer[] arrayNo = Caracteristica.arrayCarNo();

		if(arrayNo[number]!=null)
		{
			switch(number){
			case 0: Caracteristica.carAfirmativas.add(1); crearVista(1); break;//no hombre --> mujer
			case 4: Caracteristica.carAfirmativas.add(5); crearVista(5); break;//no pelo largo --> corto
			case 10: Caracteristica.carAfirmativas.add(11); crearVista(11); break;//no casta�o --> color poco com�n
			case 12: Caracteristica.carAfirmativas.add(13); crearVista(13); break;//no ojos claros --> oscuros
			case 14: Caracteristica.carAfirmativas.add(15); crearVista(15); break;//no alto --> bajo
			case 17: Caracteristica.carAfirmativas.add(18); crearVista(18); break;//no complexi�n normal --> delgado
			}
			ejecutaConsulta(arrayNo[number]);
		}
		else
		{
			ejecutaConsulta(number+1);
		}

	}

	/**
	 * "NoSe" Logic for indeterminate characteristics to establish their subsequent behavior
	 * @param number
	 */
	public void logicaNose(int number)
	{

		Integer[] arrayNoSe = Caracteristica.arrayCarNo();

		if(arrayNoSe[number]!=null)
		{
			if(arrayNoSe[number]==Caracteristica.NO_ADIVINADO)
			{
				try {
					out.writeUTF(Constantes.NO_RESULT);
				} catch (IOException e) {
					Logger.getLogger(getClass().getName()).log(Level.SEVERE, "ERROR al enviar NO_RESULT: " + e.getMessage());
				}
			}
			else
			{
				ejecutaConsulta(arrayNoSe[number]);
			}
		}
		else
		{
			ejecutaConsulta (number+1);
		}

	}

	/**
	 * Inserts or updates the characters in the database, which is set in the client dialog when he does not 
	 * guess the solution
	 */
	public void insertarPersonaje() {
		String afirNeg, nombre, apellido;
		try {
			//Si recibimos respuesta
			if((afirNeg = in.readUTF())!=null){
				if (!(afirNeg.equals(Constantes.VOID_STRING))) {
					//Si el dato recibido contiene la palabra nombre querr� decir que recibimos el nombre y apellido del personaje
					if (afirNeg.contains(Constantes.NAME)) {
						//por tanto los recogemos
						int posN = afirNeg.indexOf("#");
						int posA = afirNeg.lastIndexOf("#");
						int posFN = afirNeg.indexOf("$");
						nombre = afirNeg.substring(posN + 1, posFN);
						apellido = afirNeg.substring(posA + 1);
						//Si no se inserta ni nombre ni apellido o se introduce nombre sin apellido o viceversa
						if ((nombre.equals(Constantes.VOID_STRING)) || (apellido.equals(Constantes.VOID_STRING)) || 
								((apellido.equals(Constantes.VOID_STRING)) && (nombre.equals(Constantes.VOID_STRING)))) {
							//Se dice que no se ha podido insertar
							Logger.getLogger(getClass().getName()).log(Level.INFO, "Could not insert anything due to lack of data");
						} else {
							//Se comprueba si existe el personaje haciendo la consulta en la base de datos con dicho nombre y apellido
							String existe = "Select per_id from adivina_personaje.personaje where nombre like '%"+nombre+"%' and apellido like '%"+apellido+"%'";
							try {
								//Se ejecuta la consulta
								stm = connection.createStatement();
								rs = stm.executeQuery(existe);
								counter = 0;
								while (rs.next()) {
									//Si se obtiene resultado lo veremos en contador
									number = rs.getInt(1);
									counter++;
								}
								//Si no existe el personaje
								if(counter == 0){
									//Se tiene que insertar tanto en la tabla Personaje como en la tabla Tener, para ello se tiene que saber cual es la identificaci�n
									//del personaje, para ello se cuentan el numero de personajes que existen para insertar el siguiente n�mero
									query = "Select count(*) from adivina_personaje.personaje";
									try {
										stm = connection.createStatement();
										rs = stm.executeQuery(query);
										while (rs.next()) {
											number = rs.getInt(1);
										}
										insertarTablaPersonaje(nombre,apellido);
										insertarTablaTener(number+1);
									} catch (SQLException e) {
										try {
											rs.close();
										} catch (SQLException e1) {
											Logger.getLogger(getClass().getName()).log(Level.SEVERE, "-->ERROR closing ResultSet: " + e1.getMessage());
										}
										Logger.getLogger(getClass().getName()).log(Level.SEVERE, "-->ERROR counting characters number: " + e.getMessage());
									}
								}else{
									//Si la consulta devuelve un resultado se actualiza el personaje, por tanto en la tabla Tener se insertan 
									//los valores correspondientes de las caracter�sticas a a�adir del personaje existente
									insertarTablaTener(number);
								}
							} catch (SQLException e) {
								try {
									rs.close();
								} catch (SQLException e1) {
									Logger.getLogger(getClass().getName()).log(Level.SEVERE, "-->ERROR closing ResultSet: " + e1.getMessage());
								}
								Logger.getLogger(getClass().getName()).log(Level.SEVERE, "-->ERROR checking for: " + e.getMessage());
							}
						}
						//Si se recibe correcto en la respuesta querr� decir que el usuario ha dicho que si es el personaje que ha pensado por tanto no es necesario
						//que insertemos nada
					} else if (afirNeg.contains(Constantes.CORRECT)) {
						Logger.getLogger(getClass().getName()).log(Level.INFO, "It has had to not insert anything because the character is right");
					}
				}
			}
		} catch (IOException e) {
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, "-->ERROR reading input: " + e.getMessage());
		}
	}

	/**
	 * Inserts in the character table the corresponding name and surname
	 * @param name
	 * @param surname
	 */
	public void insertarTablaPersonaje(String name, String surname){
		//La id ser� el n�mero de personajes existentes m�s uno
		int id = number + 1;
		try {
			//Se realiza la insercci�n con el id, el nombre y el apellido
			insert = "Insert into adivina_personaje.personaje values(?,?,?)";
			pstmt = connection.prepareStatement(insert);
			pstmt.setInt(1, id);
			pstmt.setString(2, name);//coger nombre introducido en dialogo
			pstmt.setString(3, surname);//coger apellido introducido en dialogo
			pstmt.executeUpdate();
			Logger.getLogger(getClass().getName()).log(Level.INFO, "A character is inserted with id " + id + " ,name " + name + " and surname " + surname);
		} catch (SQLException e) {
			try {
				pstmt.close();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				Logger.getLogger(getClass().getName()).log(Level.SEVERE, "-->ERROR closing PreparedStatement: " + e1.getMessage());
			}
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, "-->ERROR inserting in PERSONAJE table: " + e.getMessage());
		}	
	}

	/**
	 * Inserts in TENER table
	 * @param id
	 */
	public void insertarTablaTener(int id){
		//La id del personaje se recoge el m�todo insertarPersonaje que ser� el mismo n�mero en caso de que el personaje ya exista, y un n�mero mayor en el caso de que
		//no exista el personaje y haya que insertar sus caracter�sticas y la id de las caracter�sticas ser� las almacendas en Caracteristica.carAfirmativas m�s uno para que se 
		//correspondan con las almacenadas en la base de datos
		int id_car = 0;
		//Se recorre el ArrayList con un iterador
		Iterator<Integer> it=Caracteristica.carAfirmativas.iterator();
		//mientras tenga valores
		while(it.hasNext()){
			//la id de la caracter�stica ser� como he dicho antes el valor almacenado en el ArrayList m�s uno
			id_car = it.next();
			id_car = id_car +1;
			//Comprobamos si existe ese personaje con esa caracter�stica
			String existe = "Select * from adivina_personaje.tener where per_id = "+id+" and car_id = "+ id_car;
			try {
				stm = connection.createStatement();
				rs = stm.executeQuery(existe);
				counter = 0;
				while (rs.next()) {
					number = rs.getInt(1);
					counter++;
				}
			} catch (SQLException e2) {
				try {
					rs.close();
				} catch (SQLException e1) {
					Logger.getLogger(getClass().getName()).log(Level.SEVERE, "-->ERROR closing ResultSet: " + e1.getMessage());
				}
				Logger.getLogger(getClass().getName()).log(Level.SEVERE, "-->ERROR performing query character existence: " + e2.getMessage());
			}
			try {
				//Si no existe insertamos el id del personaje con el id de la caracter�stica correspondiente
				if(counter == 0){
					insert = "Insert into adivina_personaje.tener values(?,?)";
					pstmt = connection.prepareStatement(insert);
					pstmt.setInt(1, id);
					pstmt.setInt(2, id_car);//coger id_caracteristicas que se han almacenado en el ArrayList correspondiente
					pstmt.executeUpdate();
					Logger.getLogger(getClass().getName()).log(Level.INFO, "-->A character is inserted with id " + id + " and the characteristic " + id_car);
				}
				else
				{
					//Si existe simplemente establecemos un mensaje en el Servidor conforme ese personaje con esa caracter�stica ya existe
					Logger.getLogger(getClass().getName()).log(Level.INFO, "-->Character with " + id + " and characteristic " + id_car + " already exists");
				}
			} catch (SQLException e) {
				try {
					pstmt.close();
				} catch (SQLException e1) {
					Logger.getLogger(getClass().getName()).log(Level.SEVERE, "-->ERROR closing PreparedStatement: " + e1.getMessage());
				}
				Logger.getLogger(getClass().getName()).log(Level.SEVERE, "-->ERROR inserting in TENER table: " + e.getMessage());
			}
		}
	}

	/**
	 * Allows to show the character and inserting him it is necessary
	 * 
	 * @param number
	 */
	public void resultadoPersonaje(int number){
		String resultadoA[];
		resultadoA = new String[2];
		//Enviamos la palabra clave "person" para que el Cliente sepa que se ha adivinado el personaje
		try {
			out.writeUTF(Constantes.PERSON);
		} catch (IOException e1) {
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, "-->ERROR al enviar 'PERSON': " + e1.getMessage());
		}
		try {
			//Ejecutamos la consulta con el n�mero correspondiente al resultado de las vistas
			query = "Select nombre, apellido from adivina_personaje.personaje where per_id = "+number;
			rs = stm.executeQuery(query);
			while (rs.next()) {
				resultadoA[0] = rs.getString(1);
				resultadoA[1] = rs.getString(2);
			}
			try {
				//Enviamos el nombre y apellido del personaje para que se muestre por pantalla en la parte del Cliente
				out.writeUTF(resultadoA[0] + " " + resultadoA[1]);
			} catch (IOException e) {
				Logger.getLogger(getClass().getName()).log(Level.SEVERE, "-->ERROR sending the consulted character: " + e.getMessage());
			}
			//Si no es el personaje correcto se tendr� que insertar y adem�s eliminar el ArrayList de las caracter�sticas para futuras jugadas
			insertarPersonaje();
			Caracteristica.carAfirmativas.clear();
		} catch (SQLException e) {
			try {
				rs.close();
			} catch (SQLException e1) {
				Logger.getLogger(getClass().getName()).log(Level.SEVERE, "-->ERROR closing ResultSet: " + e1.getMessage());
			}
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, "-->ERROR, it could not return the result with name and surname of the character: " + e.getMessage());
		}
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			//Primero, aceptamos el Cliente
			client = server.accept();
			Logger.getLogger(getClass().getName()).log(Level.INFO, "-->Player connected");
			//Creamos la entrada y la salida
			out = new DataOutputStream(client.getOutputStream());
			in = new DataInputStream(client.getInputStream());
			//Conectamos el Servidor con la base de datos MySQL con su usuario y contrase�a correspondientes
			Logger.getLogger(getClass().getName()).log(Level.INFO, "-->Creating connection with database...");
			try {
				DriverManager.registerDriver(new com.mysql.jdbc.Driver());
				connection = DriverManager.getConnection(Constantes.RUTE_JDBC, Constantes.USER, Constantes.PASSWORD);
			} catch (SQLException e) {
				Logger.getLogger(getClass().getName()).log(Level.SEVERE, "-->ERROR connecting with database: " + e.getMessage());
			}
			//Se crea un Array de consultas de caracter�sticas de tal manera que en el momento que se inicia el juego est�n a disposici�n del Servidor para 
			//mostrarlas al Cliente
			querY = new String[Constantes.NUM_CHARAC];
			for (int i = 0; i < Constantes.NUM_CHARAC; i++) {
				querY[i] = "Select nombre from caracteristica where car_id = " + (i + 1);
			}
			//Iniciamos la consulta con la caracter�stica hombre
			consulta(0);
		} catch (IOException e) {
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, "-->ERROR connecting the player: " + e.getMessage());
		}
	}

}
