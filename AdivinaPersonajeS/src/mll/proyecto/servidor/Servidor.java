package mll.proyecto.servidor;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author marta.losada
 */
public class Servidor {

	private ServerSocket servidor;
	private Hilo hilo[];

	/**
	 * Server constructor which initializes the threads and the Socket
	 */
	public Servidor() {
		Logger.getLogger(getClass().getName()).log(Level.INFO, "-->>CREATING SERVER<<--");
		try {
			//El Servidor a la espera en el puerto 2812
			servidor = new ServerSocket(2812);
			Logger.getLogger(getClass().getName()).log(Level.INFO, "-->Waiting for player...");
			//Ejecutamos los Hilos
			Logger.getLogger(getClass().getName()).log(Level.INFO, "-->>CREATING THREADS<<--");
			hilo = new Hilo[100];
			for (int i = 0; i < 100; i++) {
				hilo[i] = new Hilo(servidor);
//				new Thread(hilo[i]).start();
				Thread thread = new Thread(hilo[i]);
				thread.start();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Server execution
	 * @param args
	 */
	public static void main(String[] args) {
		new Servidor();
	}

}
